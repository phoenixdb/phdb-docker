FROM ubuntu:vivid

RUN echo deb http://packages.phoenixdb.org/deb vivid qa > /etc/apt/sources.list.d/phoenixdb.list

RUN apt-get update && apt-get install -y --force-yes \
    phoenixdb-all \
    mpich \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

ENV DATA_DIR /data
ENV PHDB_CATALOG_HOST phdb_catalog
ENV PHDB_CATALOG_DB phdb_catalog
ENV POSTGRES_USER postgres
ENV POSTGRES_PASSWORD postgres_password
ENV PHDB_CATALOG_USER phdb
ENV PHDB_CATALOG_PASSWORD phdb_password

VOLUME /data

EXPOSE 1239

RUN mkdir /default_configs

COPY phoenixdb.conf /default_configs
COPY log4cxx.properties /default_configs

COPY entrypoint.sh /
ENTRYPOINT ["/entrypoint.sh"]

CMD ["start"]
