# Docker image for PhoenixDB

## How to use this image

1 Create new network
```
#!shell
sudo docker network create phdb_net
```

2 Run PostgreSQL container
```
#!shell
sudo docker run --name=phdb_catalog -v /data/catalog:/var/lib/postgresql/data -e POSTGRES_PASSWORD=postgres_password --net=phdb_net --rm -it postgres
```

3 Run PhoenixDB container
```
#!shell
sudo docker run --name phdb --net=phdb_net -v /data/instance:/data -e POSTGRES_PASSWORD=postgres_password --rm -it phdb/phdb start
```

4 Now you can connect to phdb through iaql
```
#!shell
sudo docker run -net=phdb_net --rm -it phdb/phdb sh -c 'exec iaql -c phdb'
```

## Environment Variables

### DATA_DIR

Optional environment variable to define location of data and config of instance. Default is **/data**.

### PHDB_CATALOG_HOST

Optional environment variable to define host with PostgreSQL. Default is **phdb_catalog**.

### PHDB_CATALOG_DB

Optional environment variable to define system catalog database. Default is **phdb_catalog**.

### POSTGRES_USER

Optional environment variable to define PostgreSQL adminstrator role. Default is **postgres**.

### POSTGRES_PASSWORD

Optional environment variable to define system catalog password. Default is **postgres_password**.

### PHDB_CATALOG_USER

Optional environment variable to define system catalog owner role. Default is **phdb**.

### PHDB_CATALOG_PASSWORD

Optional environment variable to define system catalog owner password. Default is **phdb_password**.
