#!/bin/bash
set -e

mkdir -p $DATA_DIR
cd $DATA_DIR

IP=`awk 'NR==1 {print $1}' /etc/hosts`
COMMON_ARGS=(-f phoenixdb.conf -s ${DATA_DIR}/storage.phdb -i $IP -p 1239)
LOG_ARGS=(-l log4cxx.properties)

function initialize()
{
	echo Initializing system catalog...
    /usr/bin/phoenixdb ${COMMON_ARGS[@]} --initialize	
}

function register()
{
	echo Registering new PhDB instance...
    /usr/bin/phoenixdb ${COMMON_ARGS[@]} --register	
    touch ready
}

function start()
{
	echo Starting PhDB...
    /usr/bin/phoenixdb ${COMMON_ARGS[@]} ${LOG_ARGS[@]}	
}

NEED_INITIALIZING=0
function prepare()
{
	echo Checking configs...
	if [ ! -f "phoenixdb.conf" ]; then
		echo phoenixdb.conf not found. Will generate new
		cp /default_configs/phoenixdb.conf .
		sed -e "s/PHDB_CATALOG_HOST/$PHDB_CATALOG_HOST/g" \
			-e "s/PHDB_CATALOG_USER/$PHDB_CATALOG_USER/g" \
			-e "s/PHDB_CATALOG_DB/$PHDB_CATALOG_DB/g" \
			-e "s/PHDB_CATALOG_PASSWORD/$PHDB_CATALOG_PASSWORD/g" \
			-e "s/CONTAINER_HOST/`hostname`/g" \
			-i phoenixdb.conf
	fi

	if [ ! -f "log4cxx.properties" ]; then
		echo log4cxx.properties not found. Will generate new
		cp /default_configs/log4cxx.properties .
	fi

	echo Checking role $PHDB_CATALOG_USER...
	res=`PGPASSWORD=$POSTGRES_PASSWORD psql -t -h $PHDB_CATALOG_HOST -U $POSTGRES_USER -c "select count(*) from pg_catalog.pg_user where usename='$PHDB_CATALOG_USER';"`
	res=`echo $res | tr -d '[[:space:]]'`

	if [ "$res" != "1" ]; then
		echo "Not found. Creating role $PHDB_CATALOG_USER..."
		PGPASSWORD=$POSTGRES_PASSWORD psql -h $PHDB_CATALOG_HOST -U $POSTGRES_USER -c "create user $PHDB_CATALOG_USER with password '$PHDB_CATALOG_PASSWORD';"
	fi	

	echo Checking database $PHDB_CATALOG_DB...
	res=`PGPASSWORD=$POSTGRES_PASSWORD psql -t -h $PHDB_CATALOG_HOST -U $POSTGRES_USER -c "SELECT count(*) FROM pg_database WHERE datistemplate = false AND datname = '$PHDB_CATALOG_DB';"`	
	res=`echo $res | tr -d '[[:space:]]'`

	if [ "$res" != "1" ]; then
		echo "Not found. Creating database $PHDB_CATALOG_DB..."
		PGPASSWORD=$POSTGRES_PASSWORD createdb -h $PHDB_CATALOG_HOST -U $POSTGRES_USER -O $PHDB_CATALOG_USER $PHDB_CATALOG_DB
		NEED_INITIALIZING=1
	fi
	touch ready
}

if [ "$1" = 'initialize' ]; then
	[ -e "ready" ] || prepare
	initialize
	touch initialized
    exit 0
elif [ "$1" = 'register' ]; then
	[ -e "ready" ] || prepare
	register
    exit 0
elif [ "$1" = 'start' ]; then
	if [ ! -e "ready" ]; then
		prepare
		if [ "$NEED_INITIALIZING" == "1" ]; then
			initialize
		fi
		register
	fi
	start
	exit 0
fi

exec "$@"
